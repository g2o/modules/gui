#ifndef _VIEW_H
#define _VIEW_H

#ifdef GUI_EXPORT
#define GUI_API __declspec(dllexport)
#else
#define GUI_API __declspec(dllimport)
#endif

class GUI_API CView : protected zCView
{
public:
	CView();

	bool getVisible();
	void setVisible(bool visible);

	void top();

protected:
	bool _visible;
};

#endif
