#ifndef _ITEM_RENDER_H
#define _ITEM_RENDER_H

#include "CView2d.h"

class GUI_API CItemRender : private CView2d
{
public:
	CItemRender(int x, int y, int width, int height, SQChar* instance);
	~CItemRender();

	Sqrat::Table getPosition();
	void setPosition(int x, int y);

	Sqrat::Table getPositionPx();
	void setPositionPx(int x, int y);

	Sqrat::Table getSize();
	void setSize(int width, int height);

	Sqrat::Table getSizePx();
	void setSizePx(int width, int height);

	SQChar* getInstance();
	void setInstance(SQChar* instance);

	SQInteger getRotX();
	void setRotX(int rotX);

	SQInteger getRotY();
	void setRotY(int rotY);

	SQInteger getRotZ();
	void setRotZ(int rotZ);

	SQInteger getZBias();
	void setZBias(int zBias);

	SQInteger getLightRange();
	void setLightRange(int lightRange);

	bool getLightingSwell();
	void setLightingSwell(bool lightingSwell);

	SQChar* getVisual();
	void setVisual(SQChar* visual);

	// Inherited (Required because Sqrat throws assertion if bound member method is inherited and not implemented)

	bool getVisible() { return CView::getVisible(); }
	void setVisible(bool visible) { CView::setVisible(visible); }

	void top() { CView::Top(); }

private:
	zCVob* _cameraVob;
	int _lightRange;
	bool _lightingSwell;
	SQChar* _instance;
	oCItem* _item;

	static zCWorld* _renderWorld;
	static zCCamera* _renderCamera;

	virtual void Blit() override;
};

#endif