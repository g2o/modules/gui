#ifndef _LINE_2D_H
#define _LINE_2D_H

#include "CView2d.h"

class GUI_API CLine2d : private CView2d
{
public:
	CLine2d(int beginX, int beginY, int endX, int endY);

	Sqrat::Table getBegin();
	void setBegin(int x, int y);

	Sqrat::Table getEnd();
	void setEnd(int x, int y);

	Sqrat::Table getColor();
	void setColor(unsigned char r, unsigned char g, unsigned char b);

	// Inherited (Required because Sqrat throws assertion if bound member method is inherited and not implemented)

	bool getVisible() { return CView::getVisible(); }
	void setVisible(bool visible) { CView::setVisible(visible); }

	void top() { CView::Top(); }

private:
	virtual void Blit() override;
};

#endif