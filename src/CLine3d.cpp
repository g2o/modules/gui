#include "CLine3d.h"

using namespace sqModule;

CLine3d::CLine3d(int beginX, int beginY, int beginZ, int endX, int endY, int endZ) : CView(),
	_begin(beginX, beginY, beginZ),
	_end(endX, endY, endZ)
{
	setColor(255, 255, 0);
}

Sqrat::Table CLine3d::getBegin()
{
	return Sqrat::Table(vm)
		.SetValue("x", _begin[VX])
		.SetValue("y", _begin[VY])
		.SetValue("z", _begin[VZ]); 
}

void CLine3d::setBegin(SQFloat x, SQFloat y, SQFloat z)
{
	_begin[VX] = x;
	_begin[VY] = y;
	_begin[VZ] = z;
}

Sqrat::Table CLine3d::getEnd()
{
	return Sqrat::Table(vm)
		.SetValue("x", _end[VX])
		.SetValue("y", _end[VY])
		.SetValue("z", _end[VZ]);
}

void CLine3d::setEnd(SQFloat x, SQFloat y, SQFloat z)
{
	_end[VX] = x;
	_end[VY] = y;
	_end[VZ] = z;
}

Sqrat::Table CLine3d::getColor()
{
	return Sqrat::Table(vm)
		.SetValue("r", color.r)
		.SetValue("g", color.g)
		.SetValue("b", color.b);
}

void CLine3d::setColor(unsigned char r, unsigned char g, unsigned char b)
{
	color.SetRGB(r, g, b);
}

void CLine3d::Blit()
{
	if (!_visible)
		return;

	zCCamera* camera = zCCamera::activeCam;

	if (!camera)
		return;

	if (!camera->connectedVob)
		return;

	const zMAT4& camMatrix = camera->connectedVob->trafoObjToWorld.InverseLinTrafo();

	zVEC3 beginTransformed = camMatrix * _begin;
	zVEC3 endTransformed = camMatrix * _end;

	// If both points of the line are behind the camera, do not render it
	if (beginTransformed[VZ] <= 0.0f && endTransformed[VZ] <= 0.0f)
		return;

	Alg_ClipAtZ0(beginTransformed, endTransformed);

	zVEC2 screenBegin, screenEnd;

	camera->Project(&beginTransformed, screenBegin[VX], screenBegin[VY]);
	camera->Project(&endTransformed, screenEnd[VX], screenEnd[VY]);

	int x1 = screenBegin[VX];
	int y1 = screenBegin[VY];
	int x2 = screenEnd[VX];
	int y2 = screenEnd[VY];

	// Clip line to fit screen frame
	if (!screen->ClipLine(x1, y1, x2, y2))
		return;

	zrenderer->DrawLine(x1, y1, x2, y2, color);
}

