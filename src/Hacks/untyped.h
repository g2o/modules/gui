#ifndef _UNTYPED_H
#define _UNTYPED_H

struct untyped
{
	void *data;

	untyped()
	{
		data = nullptr;
	}

	template <class T>
	untyped(T x)
	{
		data = reinterpret_cast<void *&>(x);
	}

	operator void*() const
	{
		return data;
	}
};

#endif