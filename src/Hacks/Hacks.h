#ifndef HACKS_H
#define HACKS_H

#include <cstring> // memcpy, std::size_t
#include <windows.h> // WinAPI functions
#include <vector> // used to represents bytes of unknown size in CMemoryPatch

#include "untyped.h"
#include "CHook.h"
#include "CMemoryPatch.h"

#define HOOK auto
#define AS = CreateHook

#endif
