#ifndef _CHOOK_H
#define _CHOOK_H

#include <cstring> // memcpy, std::size_t
#include <windows.h> // WinAPI functions

#include "UNTYPED.h"

template <class T>
class CHook
{
private:
	void* address;
	void* detour;

	BYTE backup[5];
	DWORD protection = 0;

public:
	CHook(const untyped address, const untyped detour)
	{
		this->address = address;
		this->detour = detour;

		Attach();
	}

	~CHook()
	{		
		Detach();
	}

	bool Attach()
	{
		BYTE bytes[] =
		{
			0xE9, //JMP op code
			0x00, 0x00, 0x00, 0x00 //relative address
		};

		// making backup of original 5 bytes
		memcpy(&backup, address, 5);

		// appling hook
		std::size_t offset = ((std::size_t)detour - (std::size_t)address - 5); //calculating relative address
		memcpy(&bytes[1], &offset, 4); //writing address

		// patching memory
		Unprotect();
		memcpy(address, bytes, 5);
		Protect();

		// flushing CPU cache
		FlushInstructionCache(GetCurrentProcess(), NULL, NULL);

		return true;
	}

	void Detach()
	{
		Unprotect();
		WriteProcessMemory(GetCurrentProcess(), address, backup, 5, 0);
		Protect();
	}

	void Protect()
	{
		if (IsProtected())
			return;

		VirtualProtect(address, 5, protection, &protection);
		protection = 0;
	}

	void Unprotect()
	{
		if (!IsProtected())
			return;

		VirtualProtect(address, 5, PAGE_EXECUTE_READWRITE, &protection);
	}

	bool IsProtected()
	{
		return !protection;
	}

	operator T()
	{
		return *(T)(address);
	}
};

template <class T>
static inline CHook<T> CreateHook(const untyped& adr, T ptr)
{
	return CHook<T>(adr, ptr);
}

#endif