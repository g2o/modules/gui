#include "CView.h"

CView::CView() : zCView(0, 0, 0, 0),
	_visible(false)
{
	alphafunc = zTRnd_AlphaBlendFunc::zRND_ALPHA_FUNC_BLEND;
	screen->InsertItem(this);
}

bool CView::getVisible()
{
	return _visible;
}

void CView::setVisible(bool visible)
{
	_visible = visible;
}

void CView::top()
{
	Top();
}
