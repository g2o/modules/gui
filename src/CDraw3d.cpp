#include "CDraw3d.h"

using namespace sqModule;

constexpr const int ASCII_SPACE = 32;

CDraw3d::CDraw3d(float x, float y, float z) : CView2d(-1, -1),
	_position(x, y, z),
	_distance(1000)
{
	SetFont("FONT_OLD_10_WHITE_HI.TGA");
}

Sqrat::Table CDraw3d::getWorldPosition()
{
	return Sqrat::Table(vm)
		.SetValue("x", _position[VX])
		.SetValue("y", _position[VY])
		.SetValue("z", _position[VZ]);
}

void CDraw3d::setWorldPosition(float x, float y, float z)
{
	_position[VX] = x;
	_position[VY] = y;
	_position[VZ] = z;
}

Sqrat::Table CDraw3d::getColor()
{
	return Sqrat::Table(vm)
		.SetValue("r", color.r)
		.SetValue("g", color.g)
		.SetValue("b", color.b);
}

void CDraw3d::setColor(unsigned char r, unsigned char g, unsigned char b)
{
	color.SetRGB(r, g, b);
}

unsigned char CDraw3d::getAlpha()
{
	return color.a;
}

void CDraw3d::setAlpha(unsigned char alpha)
{
	color.a = alpha;
}

void CDraw3d::insertText(SQChar* text)
{
	_text.push_back(text);
	updateSize();
}

void CDraw3d::removeText(SQInteger position)
{
	if (position < 0)
		return;

	if (position >= _text.size())
		return;

	_text.erase(_text.begin() + position);
	updateSize();
}

void CDraw3d::updateText(SQInteger position, SQChar* text)
{
	if (position <= 0)
		return;

	if (position >= _text.size())
		return;

	_text[position] = text;
	updateSize();
}

Sqrat::Array CDraw3d::getText()
{
	Sqrat::Array result(vm);

	for (auto text : _text)
		result.Append(text);

	return result;
}

SQInteger CDraw3d::getDistance()
{
	return _distance;
}

void CDraw3d::setDistance(SQInteger distance)
{
	_distance = distance;
}

SQChar* CDraw3d::getFont()
{
	if (!font)
		return "";

	zCTexture* fontTex = font->GetFontTexture();

	if (!fontTex)
		return "";

	return fontTex->GetObjectName().ToChar();
}

void CDraw3d::setFont(SQChar* fontName)
{
	SetFont(fontName);
	updateSize();
}

void CDraw3d::updateSize()
{
	int letterDistance = font->GetLetterDistance();
	int spaceWidth = font->GetWidth(' ');

	int pixelWidth = 0;
	int pixelHeight = 0;

	for (std::string& text : _text)
	{
		int lineWidth = 0;

		for (char& ch : text)
		{
			if (ch != ASCII_SPACE)
				lineWidth += font->GetWidth(ch) + letterDistance;
			else
				lineWidth += spaceWidth;
		}

		if (lineWidth > pixelWidth)
		{
			pixelWidth = lineWidth;
			lineWidth = 0;
		}

		pixelHeight += font->GetFontY();
	}

	setPixelWidth(pixelWidth);
	setPixelHeight(pixelHeight);
}

void CDraw3d::Blit()
{
	if (!font)
		return;

	if (!_visible)
		return;

	if (player->GetPositionWorld().Distance(_position) > _distance)
		return;

	zCCamera* camera = zCCamera::activeCam;

	if (!camera)
		return;

	zBOOL oldzWrite = zrenderer->GetZBufferWriteEnabled();
	zrenderer->SetZBufferWriteEnabled(FALSE);

	zTRnd_ZBufferCmp oldCmp = zrenderer->GetZBufferCompare();
	zrenderer->SetZBufferCompare(zRND_ZBUFFER_CMP_ALWAYS);

	zTRnd_AlphaBlendFunc oldBlendFunc = zrenderer->GetAlphaBlendFunc();
	zrenderer->SetAlphaBlendFunc(alphafunc);

	zBOOL oldBilerpFilter = zrenderer->GetBilerpFilterEnabled();
	zrenderer->SetBilerpFilterEnabled(TRUE);

	zVEC3 vecTransformed = camera->Transform(_position);

	if (vecTransformed[VZ] < 0.0f)
		return;

	int x, y;
	camera->Project(&vecTransformed, x, y);

	// if element is outside the screen -> break
	if (x > zrenderer->vid_xdim - 1 || y > zrenderer->vid_ydim - 1)
		return;

	if (x + getPixelWidth() < 0 || y + getPixelHeight() < 0)
		return;

	int letterDistance = font->GetLetterDistance();
	int spaceWidth = font->GetWidth(' ');

	int fontHeight = font->GetFontY();
	zCTexture* fontTex = font->GetFontTexture();

	zREAL farZ = (zCCamera::activeCam) ? zCCamera::activeCam->nearClipZ + 1 : 1;

	for (std::string& text : _text)
	{
		int lineX = x;

		for (char& ch : text)
		{
			if (ch != ASCII_SPACE)
			{
				zVEC2 uvPos, uvSize;
				int letterWidth;

				font->GetFontData(ch, letterWidth, uvPos, uvSize);

				zVEC2 letterPosMin(lineX, y);
				zVEC2 letterPosMax(lineX + letterWidth - 1, y + fontHeight - 1);

				// if letter is outside the screen (RIGHT SIDE on X axis) -> break the loop (this is good because draws doesn't support EOL aka \n character)
				if (letterPosMin[VX] > zrenderer->vid_xdim - 1)
					break;

				// if letter isn't outside the screen (LEFT SIDE on X axis) -> render it
				if (letterPosMax[VX] >= 0)
					zrenderer->DrawTile(fontTex, letterPosMin, letterPosMax, farZ, uvPos, uvSize, color);

				lineX += letterWidth + letterDistance;
			}
			else
				lineX += spaceWidth;
		}

		y += font->GetFontY();
	}

	// Restoring old values (defaults in zrenderer)
	zrenderer->SetBilerpFilterEnabled(oldBilerpFilter);
	zrenderer->SetAlphaBlendFunc(oldBlendFunc);
	zrenderer->SetZBufferWriteEnabled(oldzWrite);
	zrenderer->SetZBufferCompare(oldCmp);
}
