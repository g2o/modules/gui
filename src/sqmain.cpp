#include "pch.h"

#include "CDraw.h"
#include "CDraw3d.h"
#include "CTexture.h"
#include "CLine2d.h"
#include "CLine3d.h"
#include "CItemRender.h"

#include "Hacks/Hacks.h"

// BytePatch fixes for alpha rendering problems when opening GameMenu, StatusScreen and LogScreen
// The problem is that G2O calls oCGame::Unpause per frame to prevent from pausing game.
// CGameManager::Menu calls zCView::DrawItems which is the main problem?
// Those fixes prevents from occuring the problem, the source of the problem is still unknown
// The key to cause this bug is to call oCGame::Pause and in next frame call oCGame::UnPause

CMemoryPatch Patch_CGameManager__Menu_Pause(0x00429678, { 0x90, 0x90, 0x90, 0x90 });
CMemoryPatch Patch_CGameManager__Menu_Draw_Items(0x00429779, { 0x90, 0x90, 0x90 });
CMemoryPatch Patch_oCStatusScreen__Show_Pause(0x0047F4D7, { 0x90, 0x90, 0x90, 0x90, 0x90 });
CMemoryPatch Patch_oCLogScreen__Show_Pause(0x0047EE57, { 0x90, 0x90, 0x90, 0x90, 0x90 });

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	// Module Init
	sqModule::vm = vm;
	sqModule::api = api;

	sqModule::print = sq_getprintfunc(vm);
	sqModule::error = sq_geterrorfunc(vm);

	Sqrat::DefaultVM::Set(vm);

	// Draw Registration
	Sqrat::RootTable(vm).Bind("Draw", Sqrat::Class<CDraw>(vm, "Draw")
		.Ctor<SQInteger, SQInteger, SQChar*>()
		.Func("getPosition", &CDraw::getPosition)
		.Func("setPosition", &CDraw::setPosition)
		.Func("getPositionPx", &CDraw::getPositionPx)
		.Func("setPositionPx", &CDraw::setPositionPx)
		.Prop("width", &CDraw::getWidth)
		.Prop("widthPx", &CDraw::getWidthPx)
		.Prop("height", &CDraw::getHeight)
		.Prop("heightPx", &CDraw::getHeightPx)
		.Func("getColor", &CDraw::getColor)
		.Func("setColor", &CDraw::setColor)
		.Func("getAlpha", &CDraw::getAlpha)
		.Func("setAlpha", &CDraw::setAlpha)
		.Prop("text", &CDraw::getText, &CDraw::setText)
		.Prop("font", &CDraw::getFont, &CDraw::setFont)
		.Prop("visible", &CDraw::getVisible, &CDraw::setVisible)
		.Func("top", &CDraw::top)
	);

	// Draw3d Registration
	Sqrat::RootTable(vm).Bind("Draw3d", Sqrat::Class<CDraw3d>(vm, "Draw3d")
		.Ctor<SQInteger, SQInteger, SQInteger>()
		.Func("getWorldPosition", &CDraw3d::getWorldPosition)
		.Func("setWorldPosition", &CDraw3d::setWorldPosition)
		.Func("getColor", &CDraw3d::getColor)
		.Func("setColor", &CDraw3d::setColor)
		.Func("getAlpha", &CDraw3d::getAlpha)
		.Func("setAlpha", &CDraw3d::setAlpha)
		.Func("insertText", &CDraw3d::insertText)
		.Func("removeText", &CDraw3d::removeText)
		.Func("updateText", &CDraw3d::updateText)
		.Func("getText", &CDraw3d::getText)
		.Prop("font", &CDraw3d::getFont, &CDraw3d::setFont)
		.Prop("distance", &CDraw3d::getDistance, &CDraw3d::setDistance)
		.Prop("visible", &CDraw3d::getVisible, &CDraw3d::setVisible)
		.Func("top", &CDraw::top)
	);

	// Texture Registration
	Sqrat::RootTable(vm).Bind("Texture", Sqrat::Class<CTexture>(vm, "Texture")
		.Ctor<SQInteger, SQInteger, SQInteger, SQInteger, SQChar*>()
		.Func("getPosition", &CTexture::getPosition)
		.Func("setPosition", &CTexture::setPosition)
		.Func("getPositionPx", &CTexture::getPositionPx)
		.Func("setPositionPx", &CTexture::setPositionPx)
		.Func("getSize", &CTexture::getSize)
		.Func("setSize", &CTexture::setSize)
		.Func("getRect", &CTexture::getRect)
		.Func("setRect", &CTexture::setRect)
		.Func("getRectPx", &CTexture::getRectPx)
		.Func("setRectPx", &CTexture::setRectPx)
		.Func("getSizePx", &CTexture::getSizePx)
		.Func("setSizePx", &CTexture::setSizePx)
		.Func("getColor", &CTexture::getColor)
		.Func("setColor", &CTexture::setColor)
		.Func("getAlpha", &CTexture::getAlpha)
		.Func("setAlpha", &CTexture::setAlpha)
		.Prop("file", &CTexture::getFile, &CTexture::setFile)
		.Prop("visible", &CTexture::getVisible, &CTexture::setVisible)
		.Func("top", &CTexture::top)
	);

	// Line2d Registration
	Sqrat::RootTable(vm).Bind("Line2d", Sqrat::Class<CLine2d>(vm, "Line2d")
		.Ctor<SQInteger, SQInteger, SQInteger, SQInteger>()
		.Func("getBegin", &CLine2d::getBegin)
		.Func("setBegin", &CLine2d::setBegin)
		.Func("getEnd", &CLine2d::getEnd)
		.Func("setEnd", &CLine2d::setEnd)
		.Func("getColor", &CLine2d::getColor)
		.Func("setColor", &CLine2d::setColor)
		.Prop("visible", &CLine2d::getVisible, &CLine2d::setVisible)
		.Func("top", &CLine2d::top)
	);

	// Line3d Registration
	Sqrat::RootTable(vm).Bind("Line3d", Sqrat::Class<CLine3d>(vm, "Line3d")
		.Ctor<SQFloat, SQFloat, SQFloat, SQFloat, SQFloat, SQFloat>()
		.Func("getBegin", &CLine3d::getBegin)
		.Func("setBegin", &CLine3d::setBegin)
		.Func("getEnd", &CLine3d::getEnd)
		.Func("setEnd", &CLine3d::setEnd)
		.Func("getColor", &CLine3d::getColor)
		.Func("setColor", &CLine3d::setColor)
		.Prop("visible", &CLine3d::getVisible, &CLine3d::setVisible)
		.Func("top", &CLine3d::top)
	);

	// ItemRender Registration
	Sqrat::RootTable(vm).Bind("ItemRender", Sqrat::Class<CItemRender>(vm, "ItemRender")
		.Ctor<SQInteger, SQInteger, SQInteger, SQInteger, SQChar*>()
		.Func("getPosition", &CItemRender::getPosition)
		.Func("setPosition", &CItemRender::setPosition)
		.Func("getPositionPx", &CItemRender::getPositionPx)
		.Func("setPositionPx", &CItemRender::setPositionPx)
		.Func("getSize", &CItemRender::getSize)
		.Func("setSize", &CItemRender::setSize)
		.Func("getSizePx", &CItemRender::getSizePx)
		.Func("setSizePx", &CItemRender::setSizePx)
		.Prop("instance", &CItemRender::getInstance, &CItemRender::setInstance)
		.Prop("rotX", &CItemRender::getRotX, &CItemRender::setRotX)
		.Prop("rotY", &CItemRender::getRotY, &CItemRender::setRotY)
		.Prop("rotZ", &CItemRender::getRotZ, &CItemRender::setRotZ)
		.Prop("zbias", &CItemRender::getZBias, &CItemRender::setZBias)
		.Prop("lightrange", &CItemRender::getLightRange, &CItemRender::setLightRange)
		.Prop("lightingswell", &CItemRender::getLightingSwell, &CItemRender::setLightingSwell)
		.Prop("visible", &CItemRender::getVisible, &CItemRender::setVisible)
		.Prop("visual", &CItemRender::getVisual, &CItemRender::setVisual)
		.Func("top", &CItemRender::top)
	);

#ifdef __DEBUG
		if (
			AllocConsole() &&
			freopen("conin$", "r", stdin) &&
			freopen("conout$", "w", stdout) &&
			freopen("conout$", "w", stderr)
		)
			std::cout << "[Debug] Console initalized!\n";
#endif

	return SQ_OK;
}
