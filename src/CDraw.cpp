#include "CDraw.h"

using namespace sqModule;

constexpr const int ASCII_SPACE = 32;

CDraw::CDraw(int x, int y, SQChar* text) : CView2d(x, y),
	_text("")
{
	SetFont("FONT_OLD_10_WHITE_HI.TGA");
    setText(text);

	screen->InsertItem(this);
}

Sqrat::Table CDraw::getPosition()
{
    return Sqrat::Table(vm)
        .SetValue("x", getVirtualX())
        .SetValue("y", getVirtualY());
}

void CDraw::setPosition(int x, int y)
{
	setVirtualX(x);
	setVirtualY(y);
}

Sqrat::Table CDraw::getPositionPx()
{
    return Sqrat::Table(vm)
        .SetValue("x", getPixelX())
        .SetValue("y", getPixelY());
}

void CDraw::setPositionPx(int x, int y)
{
	setPixelX(x);
	setPixelY(y);
}

int CDraw::getWidth()
{
    return getVirtualWidth();
}

int CDraw::getWidthPx()
{
    return getPixelWidth();
}

int CDraw::getHeight()
{
    return getVirtualHeight();
}

int CDraw::getHeightPx()
{
    return getPixelHeight();
}

Sqrat::Table CDraw::getColor()
{
    return Sqrat::Table(vm)
        .SetValue("r", fontColor.r)
        .SetValue("g", fontColor.g)
        .SetValue("b", fontColor.b);
}

void CDraw::setColor(unsigned char r, unsigned char g, unsigned char b)
{
    fontColor.SetRGB(r, g, b);
}

unsigned char CDraw::getAlpha()
{
    return fontColor.a;
}

void CDraw::setAlpha(unsigned char alpha)
{
    fontColor.a = alpha;
}

SQChar* CDraw::getText()
{
    return _text.ToChar();
}

void CDraw::setText(SQChar* text)
{
    _text = text;
    updateSize();
}

SQChar* CDraw::getFont()
{
	if (!font)
		return "";

	zCTexture* fontTex = font->GetFontTexture();

	if (!fontTex)
		return "";

    return fontTex->GetObjectName().ToChar();
}

void CDraw::setFont(SQChar* fontName)
{
    SetFont(fontName);
    updateSize();
}

void CDraw::updateSize()
{
	int letterDistance = font->GetLetterDistance();
	int spaceWidth = font->GetWidth(' ');

	int pixelWidth = 0;
	int pixelHeight = font->GetFontY();
	
	char* text = _text.ToChar();
	while (char ch = *text)
	{
		if (ch != ASCII_SPACE)
			pixelWidth += font->GetWidth(ch) + letterDistance;
		else
			pixelWidth += spaceWidth;

		++text;
	}

	setPixelWidth(pixelWidth);
	setPixelHeight(pixelHeight);
}

void CDraw::Blit()
{
	if (!font)
		return;

	if (_text.IsEmpty())
		return;

    if (!_visible)
        return;

	// Get positions of upper left (posMin) and lower right (posMax) vertices
	zVEC2 posMin, posMax;
	GetPixelExtends(posMin[VX], posMin[VY], posMax[VX], posMax[VY]);

	// if element is outside the screen -> break
	if (posMin[VX] > zrenderer->vid_xdim - 1 || posMin[VY] > zrenderer->vid_ydim - 1)
		return;

	if (posMax[VX] < 0 || posMax[VY] < 0)
		return;

	zBOOL oldzWrite = zrenderer->GetZBufferWriteEnabled();
	zrenderer->SetZBufferWriteEnabled(FALSE);

	zTRnd_ZBufferCmp oldCmp = zrenderer->GetZBufferCompare();
	zrenderer->SetZBufferCompare(zRND_ZBUFFER_CMP_ALWAYS);

	zTRnd_AlphaBlendFunc oldBlendFunc = zrenderer->GetAlphaBlendFunc();
	zrenderer->SetAlphaBlendFunc(alphafunc);

	zBOOL oldBilerpFilter = zrenderer->GetBilerpFilterEnabled();
	zrenderer->SetBilerpFilterEnabled(TRUE);

	int x = getPixelX();
	int y = getPixelY();

	int letterDistance = font->GetLetterDistance();
	int spaceWidth = font->GetWidth(' ');

	int fontHeight = font->GetFontY();
	zCTexture* fontTex = font->GetFontTexture();

	zREAL farZ = (zCCamera::activeCam) ? zCCamera::activeCam->nearClipZ + 1 : 1;

	char* text = _text.ToChar();
	while (char ch = *text)
	{
		if (ch != ASCII_SPACE)
		{
			zVEC2 uvPos, uvSize;
			int letterWidth;

			font->GetFontData(ch, letterWidth, uvPos, uvSize);

			zVEC2 letterPosMin(x, y);
			zVEC2 letterPosMax(x + letterWidth - 1, y + fontHeight - 1);

			// if letter is outside the screen (RIGHT SIDE on X axis) -> break the loop (this is good because draws doesn't support EOL aka \n character)
			if (letterPosMin[VX] > zrenderer->vid_xdim - 1)
				break;

			// if letter isn't outside the screen (LEFT SIDE on X axis) -> render it
			if (letterPosMax[VX] >= 0)
				zrenderer->DrawTile(fontTex, letterPosMin, letterPosMax, farZ, uvPos, uvSize, fontColor);

			x += letterWidth + letterDistance;
		}
		else
			x += spaceWidth;

		++text;
	}

	// Restoring old values (defaults in zrenderer)
	zrenderer->SetBilerpFilterEnabled(oldBilerpFilter);
	zrenderer->SetAlphaBlendFunc(oldBlendFunc);
	zrenderer->SetZBufferWriteEnabled(oldzWrite);
	zrenderer->SetZBufferCompare(oldCmp);
}

