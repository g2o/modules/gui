#include "CLine2d.h"

using namespace sqModule;

CLine2d::CLine2d(int beginX, int beginY, int endX, int endY) : CView2d(beginX, beginY)
{
    setEnd(endX, endY);
}

Sqrat::Table CLine2d::getBegin()
{
    return Sqrat::Table(vm)
        .SetValue("x", getVirtualX())
        .SetValue("y", getVirtualY());
}

void CLine2d::setBegin(int x, int y)
{
    setVirtualX(x);
    setVirtualY(y);
}

Sqrat::Table CLine2d::getEnd()
{
    return Sqrat::Table(vm)
        .SetValue("width", getVirtualWidth())
        .SetValue("height", getVirtualHeight());
}

void CLine2d::setEnd(int x, int y)
{
    setVirtualWidth(x);
    setVirtualHeight(y);
}

Sqrat::Table CLine2d::getColor()
{
    return Sqrat::Table(vm)
        .SetValue("r", color.r)
        .SetValue("g", color.g)
        .SetValue("b", color.b);
}

void CLine2d::setColor(unsigned char r, unsigned char g, unsigned char b)
{
    color.SetRGB(r, g, b);
}

void CLine2d::Blit()
{
    if (!_visible)
        return;

    int pixelX = getPixelX();
    int pixelY = getPixelY();
    int pixelWidth = getPixelWidth();
    int pixelHeight = getPixelHeight();

    zREAL beginX = min(pixelX, pixelWidth);
    zREAL beginY = min(pixelY, pixelHeight);
    zREAL endX = max(pixelX, pixelWidth);
    zREAL endY = max(pixelY, pixelHeight);

    // if element is outside the screen -> break
    if (beginX > zrenderer->vid_xdim - 1 || beginY > zrenderer->vid_ydim - 1)
        return;

    if (endX < 0 || endY < 0)
        return;

    zREAL onScreenPosMinX = max(beginX, 0);
    zREAL onScreenPosMinY = max(beginY, 0);
    zREAL onScreenPosMaxX = min(endX, zrenderer->vid_xdim - 1);
    zREAL onScreenPosMaxY = min(endY, zrenderer->vid_ydim - 1);

    // calculating element size on screen
    zREAL onScreenSizeWidth = onScreenPosMaxX - onScreenPosMinX;
    zREAL onScreenSizeHeight = onScreenPosMaxY - onScreenPosMinY;

    // if element is invisible (upperLeft == lowerRight) -> break
    if (onScreenSizeWidth <= 0 || onScreenSizeHeight <= 0)
        return;

    zrenderer->DrawLine(onScreenPosMinX, onScreenPosMinY, onScreenPosMaxX, onScreenPosMaxY, color);
}
